package com.nishantjain.dime.dime;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.parse.ParseObject;

/**
 * Created by Nishant on 6/20/2015.
 */
public class SneakPeekDialog extends DialogFragment {

    private ParseObject meetingToDisplay;
    private String status;
    private OnJoinMeetingListener listener;

    static SneakPeekDialog newInstance() {
        SneakPeekDialog f = new SneakPeekDialog();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int style = DialogFragment.STYLE_NO_TITLE, theme = 0;
        setStyle(style, theme);
    }

    public void setMeetingToDisplay(ParseObject meetingToDisplay, String status) {
        this.meetingToDisplay = meetingToDisplay;
        this.status = status;
    }

    public void setMeetingJoinListener(OnJoinMeetingListener listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_sneak_peek, container, false);

        TextView name = (TextView) v.findViewById(R.id.name);
        RoundedImageView type = (RoundedImageView) v.findViewById(R.id.type);
        TextView createdBy = (TextView) v.findViewById(R.id.createdBy);
        TextView objective = (TextView) v.findViewById(R.id.objective);
        Button btn = (Button) v.findViewById(R.id.joinButton);

        name.setText(meetingToDisplay.getString("meetingName"));
        switch (meetingToDisplay.getString("type")) {
            case "Food":
                type.setImageResource(R.drawable.pizza);
                type.setScaleType(ImageView.ScaleType.FIT_CENTER);
                type.setBackgroundColor(getResources().getColor(R.color.ForestGreen));
                break;
            case "Movie":
                type.setImageResource(R.drawable.movie);
                type.setBackgroundColor(getResources().getColor(R.color.Orchid));
                break;
        }
        createdBy.setText(meetingToDisplay.getString("createdBy"));
        objective.setText(meetingToDisplay.getString("meetingObjective"));

        if (status.equals("active")) {
            btn.setText("JOIN MEETING");
        } else {
            btn.setText("VIEW DECISION");
        }
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equals("active")) {
                    listener.onMeetingJoined(meetingToDisplay);
                } else {
                    listener.onDecisionViewButtonClicked(meetingToDisplay);
                }
                SneakPeekDialog.this.dismiss();
            }
        });

        return v;
    }

    public interface OnJoinMeetingListener {
        void onMeetingJoined(ParseObject meetingToJoin);

        void onDecisionViewButtonClicked(ParseObject meetingToDisplay);
    }
}
