package com.nishantjain.dime.dime;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by Nishant on 6/1/2015.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<CategoryCard> categoryCards = new ArrayList<>();
    private Context activityContext;
    private listClickListener clickListener;
    private boolean isDecisionView = false;
    private List<ParseObject> decisionList;
    private PieChart pieChart;

    public RecyclerAdapterForOpinion nestedRecyclerViewAdapter;
    //added view types
    private static final int TYPE_HEADER = 2;
    private static final int TYPE_ITEM = 1;


    public RecyclerAdapter(Context context, ArrayList<CategoryCard> categoryCards) {
        inflater = LayoutInflater.from(context);
        this.categoryCards = categoryCards;
        this.activityContext = context;
    }

    public RecyclerAdapter(List<ParseObject> decisionList, Context context) {
        this.activityContext = context;
        this.isDecisionView = true;
        this.decisionList = decisionList;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        Log.d("RecyclerAdapter", "OnCreateViewHolder called for position: ");
        inflater = LayoutInflater.from(viewGroup.getContext());
        if (!isDecisionView) {
            if (viewType == TYPE_HEADER) {
                View itemView = inflater.inflate(R.layout.recycler_header, viewGroup, false);
                return new RecyclerHeaderViewHolder(itemView);
            } else {
                View itemView = inflater.inflate(R.layout.card_list, viewGroup, false);
                return new OpinionViewHolder(itemView);
            }
        } else {
            View itemView = inflater.inflate(R.layout.item_view_decision_opinion, viewGroup, false);
            return new DecisionViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        Log.d("RecyclerAdapter", "OnBindViewHolder called for position: " + position);
        if (!isDecisionView) {
            if (!isPositionHeader(position)) {
                OpinionViewHolder opinionViewHolder = (OpinionViewHolder) viewHolder;
                CategoryCard current = categoryCards.get(position - 1);
                opinionViewHolder.category.setText(current.getCategory());
                switch (current.getCategory()) {
                    case "Cuisine":
                        opinionViewHolder.categoryImage.setImageResource(R.drawable.cuisine);
                        break;
                    case "Restaurant":
                        opinionViewHolder.categoryImage.setImageResource(R.drawable.restaurants);
                        break;
                    case "Date and Time":
                        opinionViewHolder.categoryImage.setImageResource(R.drawable.clock);
                        break;
                    default:
                        opinionViewHolder.categoryImage.setImageResource(R.drawable.gears);
                        break;
                }
                nestedRecyclerViewAdapter = current.getAdapterForOpinionList();
                nestedRecyclerViewAdapter.setPieChart(opinionViewHolder.pieChart);
                opinionViewHolder.recyclerOpinionView.setAdapter(nestedRecyclerViewAdapter);
                opinionViewHolder.recyclerOpinionView.setLayoutManager(new org.solovyev.android.views.llm.LinearLayoutManager(activityContext, LinearLayoutManager.VERTICAL, false));
                if (current.getOpinionListSize() > 1) {
                    displayPieChart(current, opinionViewHolder.pieChart);
                } else {
                    opinionViewHolder.pieChart.setVisibility(View.GONE);
                }
            }
        } else {
            DecisionViewHolder decisionViewHolder = (DecisionViewHolder) viewHolder;
            ParseObject current = decisionList.get(position);
            decisionViewHolder.decisionCategory.setText(current.getString("category"));
            decisionViewHolder.decisionEntry.setText(current.getString("entry"));
            if (current.getString("rationale") != null && !current.getString("rationale").isEmpty()) {
                decisionViewHolder.decisionRationale.setText(current.getString("rationale") + ". Majority Vote.");
            } else {
                decisionViewHolder.decisionRationale.setText("Majority Vote.");
            }
        }
    }

    private void displayPieChart(CategoryCard current, PieChart pieChart) {
        ArrayList<Entry> values = new ArrayList<>();
        PieData pieData;
        ArrayList<String> labels = new ArrayList<>();

        List<ParseObject> opinionList = current.getOpinionList();
        int size = opinionList.size();
        for (int i = 0; i < size; i++) {
            ParseObject temp = opinionList.get(i);
            if (temp.getInt("priority") > 0) {
                Entry tempEntry = new Entry(temp.getInt("priority"), i);
                labels.add("");
                values.add(tempEntry);
            }
        }
        PieDataSet pieDataSet = new PieDataSet(values, "label");
        pieDataSet.setDrawValues(false);
        pieDataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
        pieData = new PieData(labels, pieDataSet);
        pieChart.setDrawSliceText(false);
        pieChart.setData(pieData);
        pieChart.setDescription("");
        Legend legend = pieChart.getLegend();
        legend.setEnabled(false);
        pieChart.invalidate();
    }

    @Override
    public int getItemCount() {
        if (!isDecisionView) {
            return categoryCards.size() + 1;
        } else return decisionList.size();
    }

    //added a method that returns viewType for a given position
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    //added a method to check if given position is a header
    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    public class RecyclerHeaderViewHolder extends RecyclerView.ViewHolder {
        public RecyclerHeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class OpinionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView categoryImage;
        TextView category;
        Button addOpinion;
        RecyclerView recyclerOpinionView;
        PieChart pieChart;

        public OpinionViewHolder(View itemView) {
            super(itemView);
            category = (TextView) itemView.findViewById(R.id.category);
            categoryImage = (ImageView) itemView.findViewById(R.id.categoryImage);
            addOpinion = (Button) itemView.findViewById(R.id.addOpinion);
            recyclerOpinionView = (RecyclerView) itemView.findViewById(R.id.recyclerOpinionView);
            pieChart = (PieChart) itemView.findViewById(R.id.pieChart);

            addOpinion.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onCardAddButtonClicked(v, (getAdapterPosition() - 1));
        }
    }

    public void addTopic(CategoryCard card) {
        this.categoryCards.add(card);
        notifyItemInserted(getItemCount() - 1);
    }

    public class DecisionViewHolder extends RecyclerView.ViewHolder {

        TextView decisionEntry;
        TextView decisionCategory;
        TextView decisionRationale;

        public DecisionViewHolder(View itemView) {
            super(itemView);
            decisionEntry = (TextView) itemView.findViewById(R.id.decisionEntry);
            decisionCategory = (TextView) itemView.findViewById(R.id.decisionCategory);
            decisionRationale = (TextView) itemView.findViewById(R.id.decisionRationale);
        }
    }

    public void setListClickListener(listClickListener listener) {
        this.clickListener = listener;
    }

    public interface listClickListener {
        void onCardAddButtonClicked(View itemView, int position);
    }
}
