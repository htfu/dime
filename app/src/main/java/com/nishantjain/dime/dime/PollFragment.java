package com.nishantjain.dime.dime;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.clans.fab.FloatingActionButton;
import com.nishantjain.dime.dime.activities.MeetingActivity;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class PollFragment extends android.support.v4.app.Fragment implements TopicDialog.OnTopicCreatedListener, View.OnClickListener, RecyclerAdapter.listClickListener {

    private Context activityContext;
    private RecyclerAdapter recyclerAdapter;
    private RecyclerView recyclerView;
    private MaterialDialog dialog;

    public static ArrayList<CategoryCard> categoryCards;
    private Hashtable<String, CategoryCard> hashTableOpinions;
    private TextView emptyView;
    private static MenuItem refreshIcon;
    public static FloatingActionButton fabCreateTopic;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.Done:
                categoryCards = new ArrayList<>();
                recyclerAdapter = new RecyclerAdapter(activityContext, categoryCards);
                recyclerView.swapAdapter(recyclerAdapter, true);
                dialog = new MaterialDialog.Builder(getActivity())
                        .title("Please Wait")
                        .content("Refreshing")
                        .progress(true, 0)
                        .show();
                Application.downloadMeetingOpinionList(dialog, new Application.dataDownloadedCallback() {
                    @Override
                    public void dataAvailable() {
                        setAdapters();
                    }

                    @Override
                    public void networkError() {
                        Toast.makeText(getActivity(), "Please check your internet connection!", Toast.LENGTH_LONG).show();
                    }
                });

                Application.updateAvailable = false;
                setUpdateAvailable(false);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_poll, menu);
        refreshIcon = menu.findItem(R.id.Done);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        // move contents of screen up when soft keyboard
        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        activityContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_meeting, container, false);

        categoryCards = new ArrayList<>();
        recyclerAdapter = new RecyclerAdapter(activityContext, categoryCards);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        emptyView = (TextView) rootView.findViewById(R.id.empty_view);
        fabCreateTopic = (FloatingActionButton) rootView.findViewById(R.id.fabSubmit);

        fabCreateTopic.setOnClickListener(this);
        recyclerAdapter.setListClickListener(this);

        recyclerView.setAdapter(recyclerAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(activityContext));
        hideToolbarOnScroll(recyclerView);

        setAdapters();
        if (categoryCards.isEmpty()) {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }

        return rootView;
    }

    public static void hideToolbarOnScroll(RecyclerView recyclerView) {
        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {

            private static final int HIDE_THRESHOLD = 20;
            private int scrolledDistance = 0;
            private boolean controlsVisible = true;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (scrolledDistance > HIDE_THRESHOLD && controlsVisible) {
                    MeetingActivity.hideToolbar();
                    controlsVisible = false;
                    scrolledDistance = 0;
                } else if (scrolledDistance < -HIDE_THRESHOLD && !controlsVisible) {
                    MeetingActivity.showToolbar();
                    controlsVisible = true;
                    scrolledDistance = 0;
                }

                if ((controlsVisible && dy > 0) || (!controlsVisible && dy < 0)) {
                    scrolledDistance += dy;
                }
            }
        });
    }

    @Override
    public void onCardAddButtonClicked(View itemView, int position) {
        TopicDialog topicDialog;
        topicDialog = TopicDialog.newInstance();
        String[] categoryArray = getResources().getStringArray(R.array.category_array);
        int pos = 0;
        String currentCategory = categoryCards.get(position).getCategory();
        for (int i = 1; i < categoryArray.length; i++) {
            if (categoryArray[i].equals(currentCategory)) {
                pos = i;
            }
        }
        if (pos == 0) {
            topicDialog.initializeCategorySpinner(currentCategory);
        } else if (pos > 0) {
            topicDialog.initializeCategorySpinner(pos);
        }

        topicDialog.setCallbackListener(PollFragment.this);
        topicDialog.show(getFragmentManager(), "topic");
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fabSubmit:
                TopicDialog topicDialog;
                topicDialog = TopicDialog.newInstance();
                topicDialog.setCallbackListener(PollFragment.this);
                topicDialog.show(getFragmentManager(), "topic");
        }
    }

    @Override
    public void onTopicDialogSubmitted(ParseObject opinion) {
        String currentCategory = opinion.getString("category");
        CategoryCard currentCard;
        if (hashTableOpinions.containsKey(currentCategory)) {
            currentCard = hashTableOpinions.get(currentCategory);
            currentCard.getAdapterForOpinionList().addItem(opinion);
        } else {
            currentCard = new CategoryCard(currentCategory, opinion, activityContext);
            hashTableOpinions.put(currentCategory, currentCard);
            recyclerAdapter.addTopic(currentCard);
        }
        Application.silentNotifyChange();
        recyclerView.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
        super.onPause();
    }

    private void setAdapters() {
        List<ParseObject> opinionList = Application.currentMeeting.getList("opinionList");
        sortAndViewOpinionList(opinionList);
        hashTableOpinions = new Hashtable<>(10);
        String currentCategory;
        ParseObject currentOpinion;
        CategoryCard currentCard;
        if (opinionList != null) {
            for (int i = 0; i < opinionList.size(); i++) {
                currentCategory = opinionList.get(i).getString("category");
                currentOpinion = opinionList.get(i);
                if (hashTableOpinions.containsKey(currentCategory)) {
                    hashTableOpinions.get(currentCategory).addOpinion(currentOpinion);
                } else {
                    currentCard = new CategoryCard(currentCategory, currentOpinion, activityContext);
                    hashTableOpinions.put(currentCategory, currentCard);
                    categoryCards.add(currentCard);
                }
            }

            if (recyclerAdapter != null) {
                recyclerAdapter.notifyDataSetChanged();
            }
            for (int k = 0; k < categoryCards.size(); k++) {
                categoryCards.get(k).getAdapterForOpinionList().notifyDataSetChanged();
            }
        }
    }

    // BUBBLE SORTING opinion list according to priority and then updating
    // listView
    public void sortAndViewOpinionList(List<ParseObject> list) {
        ParseObject temp;
        if (list != null) {
            int length = list.size();

            for (int i = 0; i < (length - 1); i++) {
                for (int j = 0; j < (length - i - 1); j++) {
                    if (list.get(j).getInt("priority") < list.get(j + 1)
                            .getInt("priority")) {
                        temp = list.get(j);
                        list.set(j, list.get(j + 1));
                        list.set(j + 1, temp);
                    }
                }
            }
        }
    }

    public static void setUpdateAvailable(boolean ch) {
        if (refreshIcon != null && ch) {
            refreshIcon.setIcon(R.drawable.ic_refresh_blue);
        } else if (refreshIcon != null) {
            refreshIcon.setIcon(R.drawable.ic_refresh);
        }
    }

}