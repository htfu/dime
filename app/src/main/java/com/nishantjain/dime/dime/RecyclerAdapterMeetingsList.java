package com.nishantjain.dime.dime;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.ParseObject;

import java.util.Date;
import java.util.List;

/**
 * Created by Nishant on 6/12/2015.
 */
public class RecyclerAdapterMeetingsList extends RecyclerView.Adapter<RecyclerAdapterMeetingsList.ViewHolder> {
    private List<ParseObject> meetingList;
    private String status;
    private onListClickListener listClickListener;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public TextView textViewMeetingName;
        public ImageView imageViewPrimaryIcon;
        public ImageView imageViewSecondaryIcon;
        public TextView textViewMeetingCreator;
        public RelativeLayout primaryAction;
        public TextView textViewCreatedAt;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewMeetingName = (TextView) itemView.findViewById(R.id.meetingName);
            imageViewPrimaryIcon = (ImageView) itemView.findViewById(R.id.primaryMeetingIcon);
            imageViewSecondaryIcon = (ImageView) itemView.findViewById(R.id.secondaryMeetingIcon);
            textViewMeetingCreator = (TextView) itemView.findViewById(R.id.meetingCreator);
            primaryAction = (RelativeLayout) itemView.findViewById(R.id.primaryFunction);
            textViewCreatedAt = (TextView) itemView.findViewById(R.id.createdAt);

            if (status.equals("active")) {
                primaryAction.setOnClickListener(this);
                imageViewSecondaryIcon.setOnClickListener(this);
            } else {
                primaryAction.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View itemView) {
            if (listClickListener != null) {
                listClickListener.onMeetingListItemClicked(status, itemView, getPosition());
            }
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerAdapterMeetingsList(List<ParseObject> meetingList, String status) {
        this.meetingList = meetingList;
        this.status = status;
    }

    public void setListClickListener(onListClickListener clickListener) {
        this.listClickListener = clickListener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerAdapterMeetingsList.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                     int viewType) {
        // create a new view
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.meetings_item, parent, false);
        // set the view's size, margins, padding and layout parameters

        return new ViewHolder(itemView);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        viewHolder.textViewMeetingName.setText((String) meetingList.get(position).get("meetingName"));
        viewHolder.textViewMeetingCreator.setText((String) meetingList.get(position).get("createdBy"));

        Date date = meetingList.get(position).getCreatedAt();
        if (date != null) {
            String temp = date.toString().substring(0, 3);
            String temp2 = date.toString().substring(4, 10);
            viewHolder.textViewCreatedAt.setText(temp + ", " + temp2);
        }

        if (status.equals("active")) {
            viewHolder.imageViewPrimaryIcon.setImageResource(R.drawable.ic_group_work_grey600_48dp);
            viewHolder.imageViewSecondaryIcon.setImageResource(R.drawable.ic_delete_grey600_24dp);
        } else if (status.equals("archived")) {
            viewHolder.imageViewPrimaryIcon.setImageResource(R.drawable.ic_assessment_grey600_48dp);
            viewHolder.imageViewSecondaryIcon.setImageResource(android.R.color.transparent);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if (meetingList != null && meetingList.size() < 12) {
            return meetingList.size();
        } else if (meetingList != null) {
            return 12;
        } else {
            return 0;
        }
    }

    public interface onListClickListener {
        void onMeetingListItemClicked(String status, View view, int position);
    }
}
