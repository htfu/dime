package com.nishantjain.dime.dime;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by Nishant on 7/14/2015.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private Fragment frag1, frag2, frag3, fragment;
    private String[] TITLES;

    public ViewPagerAdapter(FragmentManager fm, String[] TITLES, Fragment frag1, Fragment frag2) {
        super(fm);
        this.TITLES = TITLES;
        this.frag1 = frag1;
        this.frag2 = frag2;
    }

    public ViewPagerAdapter(FragmentManager fm, String[] TITLES, Fragment frag1, Fragment frag2, Fragment frag3) {
        super(fm);
        this.TITLES = TITLES;
        this.frag1 = frag1;
        this.frag2 = frag2;
        this.frag3 = frag3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                fragment = frag1;
                break;
            case 1:
                fragment = frag2;
                break;
            case 2:
                fragment = frag3;
                break;
        }
        return fragment;
    }
}
