package com.nishantjain.dime.dime;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Nishant on 6/13/2015.
 */
public class DialogMeetingCreator extends DialogFragment implements View.OnClickListener {

    private String type;
    static private dialogStartMeetingListener dialogListener;
    private EditText meetingObjective;
    private EditText meetingName;

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */

    static DialogMeetingCreator newInstance(String type, dialogStartMeetingListener listener) {
        DialogMeetingCreator f = new DialogMeetingCreator();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("type", type);
        dialogListener = listener;
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getString("type");
        int style = DialogFragment.STYLE_NO_TITLE, theme = 0;
        setStyle(style, theme);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_meeting_creator, container, false);

        meetingName = (EditText) v.findViewById(R.id.dialogNameOfMeeting);
        meetingObjective = (EditText) v.findViewById(R.id.dialogObjectiveOfMeeting);
        TextView meetingType = (TextView) v.findViewById(R.id.dialogTypeOfMeeting);
        meetingType.setText(type);

        // Watch for button clicks.
        Button button = (Button) v.findViewById(R.id.dialogBtnStartMeeting);
        switch (type) {
            case "Food":
                button.setTextColor(getResources().getColor(R.color.ForestGreen));
                break;
            case "Movie":
                button.setTextColor(getResources().getColor(R.color.Orchid));
        }
        button.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.dialogBtnStartMeeting) {
            String name = meetingName.getText().toString();
            String objective = meetingObjective.getText().toString();
            if (!name.isEmpty() && !objective.isEmpty()) {
                dialogListener.onButtonClick(type, name, objective);
            } else {
                Toast.makeText(getActivity(), "Please enter the required information.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public interface dialogStartMeetingListener {
        void onButtonClick(String type, String meetingName, String meetingObjective);
    }
}
