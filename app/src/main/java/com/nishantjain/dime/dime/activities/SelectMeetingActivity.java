package com.nishantjain.dime.dime.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.nishantjain.dime.dime.Application;
import com.nishantjain.dime.dime.DialogMeetingCreator;
import com.nishantjain.dime.dime.R;
import com.nishantjain.dime.dime.RecyclerAdapterMeetingsList;
import com.nishantjain.dime.dime.SelectMeetingsFragment;
import com.nishantjain.dime.dime.ViewPagerAdapter;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.parse.ParseUser;

public class SelectMeetingActivity extends ActionBarActivity implements DialogMeetingCreator.dialogStartMeetingListener, View.OnClickListener {

    public static RecyclerAdapterMeetingsList activeMeetingsAdapter, archivedMeetingsAdapter;
    private FloatingActionMenu createMeetingMenu;
    private MaterialDialog dialog;
    private SmartTabLayout tabLayout;
    private Context activityContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityContext = this;
        if (Application.currentMeeting != null) {
            dialog = new MaterialDialog.Builder(this)
                    .title("Please Wait")
                    .content("Opening last meeting..")
                    .progress(true, 0)
                    .show();
            Application.downloadMeetingOpinionList(dialog, new Application.dataDownloadedCallback() {
                @Override
                public void dataAvailable() {
                    Intent meeting = new Intent(getApplicationContext(), MeetingActivity.class);
                    meeting.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(meeting);
                }

                @Override
                public void networkError() {
                    Toast.makeText(getApplicationContext(), "Please check your internet connection!", Toast.LENGTH_LONG).show();
                }
            });
        }

        setContentView(R.layout.activity_select_meeting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.appBar);
        createMeetingMenu = (FloatingActionMenu) findViewById(R.id.createMeetingMenu);
        FloatingActionButton fabMenuFood = (FloatingActionButton) findViewById(R.id.fabMenuFood);
        ViewPager viewPager = (ViewPager) findViewById(R.id.selectViewpager);
        tabLayout = (SmartTabLayout) findViewById(R.id.viewPagerTab);

        FragmentManager fragmentManager = getSupportFragmentManager();
        SelectMeetingsFragment activeMeetingsFragment = new SelectMeetingsFragment();
        activeMeetingsFragment.setListParameters("active");
        SelectMeetingsFragment archiveMeetingsFragment = new SelectMeetingsFragment();
        archiveMeetingsFragment.setListParameters("archived");
        String[] TITLES = {"Active", "Archive"};
        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(fragmentManager, TITLES, activeMeetingsFragment, archiveMeetingsFragment);
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setViewPager(viewPager);

        toolbar.setTitle("Meetings");
        setSupportActionBar(toolbar);
        createMeetingMenu.setClosedOnTouchOutside(false);
        createMeetingMenu.setAnimated(true);
        fabMenuFood.setOnClickListener(this);

        activeMeetingsAdapter = activeMeetingsFragment.getMeetingAdapter();
        archivedMeetingsAdapter = archiveMeetingsFragment.getMeetingAdapter();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_select_meeting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.logOut) {
            ParseUser.logOutInBackground();
            Intent mainActivity = new Intent(getApplication(), MainActivity.class);
            mainActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(mainActivity);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabMenuFood:
                DialogMeetingCreator.newInstance("Food", this).show(getFragmentManager(), "new");
                createMeetingMenu.close(true);
                break;
            case R.id.fabMenuMovie:
                createMeetingMenu.close(true);
        }
    }


    @Override
    public void onButtonClick(String type, String meetingName, String meetingObjective) {
        dialog = new MaterialDialog.Builder(activityContext)
                .title("Starting Meeting")
                .content("Please wait..")
                .progress(true, 0)
                .show();
        Application.startMeeting(type, meetingName, meetingObjective, dialog, new Application.meetingSyncedCallback() {
            @Override
            public void syncDone() {
                if (activeMeetingsAdapter != null) {
                    activeMeetingsAdapter.notifyItemInserted(Application.activeMeetings.size());
                }
                startActivity(new Intent(getApplicationContext(), MeetingActivity.class));
            }
        });
    }

    @Override
    protected void onPause() {
        if(dialog != null) {
            if(dialog.isShowing()) {
                dialog.dismiss();
            }
        }
        super.onPause();
    }
}
