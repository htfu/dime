package com.nishantjain.dime.dime;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class CreateAccount extends Activity{
	
	EditText Username, Password, ConfirmPassword;
	Editable username, password;
	Button SubmitButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.create_account);
		
		SubmitButton = (Button) findViewById(R.id.SubmitButton);
		Username = (EditText) findViewById(R.id.usernameNew);
		Password = (EditText) findViewById(R.id.passwordNew);
		ConfirmPassword = (EditText) findViewById(R.id.passwordNewConfirm);
		
		Username.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				Username.setText("");
				return false;
			}
		});
		
		Password.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				Password.setText("");
				return false;
			}
		});
		
		ConfirmPassword.setOnTouchListener(new View.OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				ConfirmPassword.setText("");
				return false;
			}
		});
		

		SubmitButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(Password.getText().toString().contentEquals(ConfirmPassword.getText().toString()))
				{
					final ProgressDialog dlg = new ProgressDialog(CreateAccount.this);
					dlg.setTitle("Please Wait: ");
					dlg.setMessage("Signing up, Please wait.");
					dlg.show();

					//New user account
					ParseUser user= new ParseUser();
					user.setUsername(Username.getText().toString());
					user.setPassword(Password.getText().toString());
					
					user.signUpInBackground(new SignUpCallback() {
						@Override
						public void done(com.parse.ParseException e) {
							dlg.dismiss();
							if(e!=null)
								Toast.makeText(CreateAccount.this, e.getMessage(), Toast.LENGTH_LONG).show();
							else
								finish();
							
						}
					});
				}
				else
				{
					Context context = getApplicationContext();
					Toast.makeText(context, Username.getText().toString(), Toast.LENGTH_SHORT).show();
					Toast.makeText(context, Password.getText().toString(), Toast.LENGTH_SHORT).show();
					Toast.makeText(context, "Passwords don't match!", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	@Override
	protected void onStop() {
		super.onStop();
		finish();
	}	

}



