package com.nishantjain.dime.dime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.nishantjain.dime.dime.activities.ContactsActivity;
import com.nishantjain.dime.dime.activities.SearchUsersActivity;
import com.parse.ParseUser;

import org.solovyev.android.views.llm.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

public class FriendsFragment extends Fragment implements View.OnClickListener {

    ArrayList<String> userList;
    ArrayList<String> inviteList;
    View rootView;
    RecyclerView userRecyclerView;
    public static FloatingActionMenu inviteFriends;
    private static RecyclerAdapterForFriends recyclerAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_friends, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.invite:
                recyclerAdapter.inviteUsers();
                FragmentManager manager = getFragmentManager();
                manager.popBackStack();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_friends,
                container, false);

        inviteFriends = (FloatingActionMenu) rootView.findViewById(R.id.findFriendsMenu);
        FloatingActionButton contacts = (FloatingActionButton) rootView.findViewById(R.id.fabContacts);
        FloatingActionButton search = (FloatingActionButton) rootView.findViewById(R.id.fabSearch);
        userRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        userRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        contacts.setOnClickListener(this);
        search.setOnClickListener(this);

        userList = new ArrayList<>();
        inviteList = new ArrayList<>();

        List<ParseUser> friends = Application.friends;
        List<ParseUser> members = Application.meetingUsers;
        List<ParseUser> users = new ArrayList<>();

        users.add(new ParseUser());
        if (members != null)
            users.addAll(members);
        users.add(new ParseUser());
        if (friends != null)
            users.addAll(friends);

        recyclerAdapter = new RecyclerAdapterForFriends(users, getActivity());
        userRecyclerView.setAdapter(recyclerAdapter);

        return rootView;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.fabContacts:
                inviteFriends.close(true);
                startActivity(new Intent(getActivity(), ContactsActivity.class));
                break;
            case R.id.fabSearch:
                inviteFriends.close(true);
                startActivityForResult(new Intent(getActivity(), SearchUsersActivity.class), 2);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == SearchUsersActivity.SEARCH_SUCCESSFUL) {
            recyclerAdapter.addFriend(SearchUsersActivity.selectedUser);
        } else if (resultCode == SearchUsersActivity.SEARCH_FAILED) {
            Toast.makeText(getActivity(), "Search Cancelled", Toast.LENGTH_SHORT).show();
        }
    }

    public static void userJoined(String username) {
        Application.findUser(username, new Application.searchSingleCallback() {
            @Override
            public void dataAvailable(ParseUser user) {
                if (recyclerAdapter != null)
                    recyclerAdapter.addMember(user);
            }
        });
    }
}