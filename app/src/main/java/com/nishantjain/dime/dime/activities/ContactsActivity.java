package com.nishantjain.dime.dime.activities;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.codinguser.android.contactpicker.ContactsPickerActivity;
import com.nishantjain.dime.dime.R;

public class ContactsActivity extends ContactsPickerActivity {

    @Override
    public void onContactNumberSelected(String contactNumber, String contactName) {
        super.onContactNumberSelected(contactNumber, contactName);
        Toast.makeText(this, contactName + ": " + contactNumber, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contact_picker, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
