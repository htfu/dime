package com.nishantjain.dime.dime.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.nishantjain.dime.dime.Application;
import com.nishantjain.dime.dime.R;
import com.parse.ParseUser;


public class MainSplashActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        if(ParseUser.getCurrentUser() != null) {
            Application.downloadUserData(new Application.dataDownloadedCallback()
            {
                @Override
                public void dataAvailable()
                {
                    startActivity(new Intent(getApplicationContext(), SelectMeetingActivity.class));
                }

                @Override
                public void networkError()
                {
                    Toast.makeText(getApplicationContext(), "Please check your internet connection!", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
