package com.nishantjain.dime.dime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.nishantjain.dime.dime.activities.DecisionViewActivity;
import com.nishantjain.dime.dime.activities.MeetingActivity;
import com.parse.ParseObject;

public class SelectMeetingsFragment extends Fragment implements RecyclerAdapterMeetingsList.onListClickListener, SneakPeekDialog.OnJoinMeetingListener {

    private String status;
    private SneakPeekDialog sneakDialog;
    private RecyclerAdapterMeetingsList meetingAdapter;
    private MaterialDialog dialog;


    public SelectMeetingsFragment() {
    }

    public void setListParameters(String status) {
        this.status = status;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            this.status = savedInstanceState.getString("status");
        }
        if (Application.currentUser == null) {
            Application.downloadUserData(new Application.dataDownloadedCallback() {
                @Override
                public void dataAvailable() {
                    if (meetingAdapter != null) {
                        meetingAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void networkError() {
                    Toast.makeText(getActivity(), "Please check your internet connection!", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_active_meetings, container, false);
        RecyclerView meetingRecyclerView = (RecyclerView) rootView.findViewById(R.id.meetingsRecyclerView);

        if (status.equals("active")) {
            meetingAdapter = new RecyclerAdapterMeetingsList(Application.activeMeetings, "active");
        } else if (status.equals("archived")) {
            meetingAdapter = new RecyclerAdapterMeetingsList(Application.archivedMeetings, "archived");
        }

        meetingAdapter.setListClickListener(SelectMeetingsFragment.this);
        meetingRecyclerView.setLayoutManager(new org.solovyev.android.views.llm.LinearLayoutManager(getActivity(), android.support.v7.widget.LinearLayoutManager.VERTICAL, false));
        meetingRecyclerView.setAdapter(meetingAdapter);

        return rootView;
    }


    @Override
    public void onMeetingListItemClicked(String status, View view, int position) {
        if (status.equals("active")) {
            if (view.getId() == R.id.primaryFunction) {
                sneakDialog = SneakPeekDialog.newInstance();
                sneakDialog.setMeetingToDisplay(Application.activeMeetings.get(position), status);
                sneakDialog.setMeetingJoinListener(SelectMeetingsFragment.this);
                sneakDialog.show(getActivity().getFragmentManager(), "sneak");
            } else {
                Application.deleteMeeting(Application.activeMeetings.get(position));
                meetingAdapter.notifyItemRemoved(position);
            }
        } else {
            sneakDialog = SneakPeekDialog.newInstance();
            sneakDialog.setMeetingToDisplay(Application.archivedMeetings.get(position), status);
            sneakDialog.setMeetingJoinListener(SelectMeetingsFragment.this);
            sneakDialog.show(getActivity().getFragmentManager(), "sneak");
        }
    }

    @Override
    public void onMeetingJoined(ParseObject meetingToJoin) {
        dialog = new MaterialDialog.Builder(getActivity())
                .title("Please Wait")
                .content("Joining Meeting")
                .progress(true, 0)
                .show();
        Application.joinMeeting(meetingToJoin, dialog, new Application.meetingSyncedCallback() {
            @Override
            public void syncDone() {
                startActivity(new Intent(getActivity(), MeetingActivity.class));
            }
        });
    }

    @Override
    public void onPause() {
        if (sneakDialog != null) {
            if (sneakDialog.isVisible()) {
                sneakDialog.dismiss();
            }
        }
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
        super.onPause();
    }

    @Override
    public void onDecisionViewButtonClicked(ParseObject meetingToDisplay) {
        Application.findDecision(meetingToDisplay, new Application.dataDownloadedCallback() {
            @Override
            public void dataAvailable() {
                startActivity(new Intent(getActivity(), DecisionViewActivity.class));
            }

            @Override
            public void networkError() {
                Toast.makeText(getActivity(), "Please check your internet connection!", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("status", this.status);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (meetingAdapter != null) {
            meetingAdapter.notifyDataSetChanged();
        }
    }

    public RecyclerAdapterMeetingsList getMeetingAdapter() {
        return this.meetingAdapter;
    }
}
