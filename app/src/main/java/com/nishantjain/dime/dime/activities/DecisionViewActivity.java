package com.nishantjain.dime.dime.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.nishantjain.dime.dime.Application;
import com.nishantjain.dime.dime.R;
import com.nishantjain.dime.dime.RecyclerAdapter;


public class DecisionViewActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decision_view);
        if (Application.decisionOpinionList == null) {
            Toast.makeText(getApplicationContext(), "No decision made.", Toast.LENGTH_SHORT).show();
        } else {
            RecyclerView recyclerDecisionView = (RecyclerView) findViewById(R.id.recyclerDecisionView);
            recyclerDecisionView.setLayoutManager(new LinearLayoutManager(this));
            RecyclerAdapter recyclerDecisionAdapter = new RecyclerAdapter(Application.decisionOpinionList, this);
            recyclerDecisionView.setAdapter(recyclerDecisionAdapter);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_decision_view, menu);
        return true;
    }
}
