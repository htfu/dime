package com.nishantjain.dime.dime;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseUser;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nishant on 8/10/2015.
 */
public class RecyclerAdapterForFriends extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ParseUser> userList;
    private List<ParseUser> inviteList;
    private Context context;
    private static final int TYPE_HEADER = 2;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_HEADING = 3;

    public RecyclerAdapterForFriends(List<ParseUser> userList, Context context) {
        this.userList = userList;
        this.context = context;
        this.inviteList = new ArrayList<>();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_header, parent, false);
            return new RecyclerHeaderViewHolder(itemView);
        } else if (viewType == TYPE_HEADING) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.heading_item_layout, parent, false);
            return new RecyclerHeadingViewHolder(itemView);
        } else {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item_layout, parent, false);
            return new UserViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (!isPositionHeader(position)) {
            if (isPositionHeading(position)) {
                final RecyclerHeadingViewHolder headingViewHolder = (RecyclerHeadingViewHolder) viewHolder;
                if (position == 1) {
                    headingViewHolder.heading.setText("Members");
                } else {
                    headingViewHolder.heading.setText("Friends");
                }
            } else {
                final UserViewHolder userViewHolder = (UserViewHolder) viewHolder;
                final ParseUser current = userList.get(position - 1);
                userViewHolder.Name.setText(current.getString("Name"));
                userViewHolder.username.setText(current.getUsername());

                if (current.getString("profilePicUrl") != null) {
                    Picasso.with(context).load(current.getString("profilePicUrl")).into(userViewHolder.userProfile);
                } else {
                    userViewHolder.userProfile.setImageResource(R.drawable.com_facebook_profile_picture_blank_portrait);
                }

                //if members already
                if (Application.meetingUsers != null && (position - 1) <= Application.meetingUsers.size()) {
                    userViewHolder.check.setVisibility(View.GONE);
                }
                //if friends
                else {
                    userViewHolder.check.setVisibility(View.VISIBLE);
                    userViewHolder.check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if (isChecked) {
                                inviteList.add(current);
                            } else {
                                inviteList.remove(current);
                            }
                        }
                    });
                    userViewHolder.primaryFunction.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (userViewHolder.check.isChecked()) {
                                userViewHolder.check.setChecked(false);
                            } else {
                                userViewHolder.check.setChecked(true);
                            }
                        }
                    });
                }
            }
        }
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {

        TextView Name, username;
        CircularImageView userProfile;
        CheckBox check;
        RelativeLayout primaryFunction;

        public UserViewHolder(View itemView) {
            super(itemView);
            Name = (TextView) itemView.findViewById(R.id.Name);
            username = (TextView) itemView.findViewById(R.id.username);
            userProfile = (CircularImageView) itemView.findViewById(R.id.userProfile);
            check = (CheckBox) itemView.findViewById(R.id.check);
            primaryFunction = (RelativeLayout) itemView.findViewById(R.id.primaryFunction);
        }
    }

    //added a method that returns viewType for a given position
    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        } else if (isPositionHeading(position)) {
            return TYPE_HEADING;
        } else {
            return TYPE_ITEM;
        }
    }

    public void inviteUsers() {
        if (inviteList != null && inviteList.size() > 0) {
            Application.notifyUsers(inviteList, "invite");
            Toast.makeText(context, "Invitations Sent!", Toast.LENGTH_SHORT).show();
        }
    }

    //added a method to check if given position is a header
    private boolean isPositionHeader(int position) {
        return position == 0 || position == getItemCount() - 1;
    }

    private boolean isPositionHeading(int position) {
        return (position - 2) < userList.size() && userList.get(position - 1).getUsername() == null;
    }

    @Override
    public int getItemCount() {
        return userList.size() + 2;
    }

    public class RecyclerHeaderViewHolder extends RecyclerView.ViewHolder {
        public RecyclerHeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    public void addFriend(ParseUser user) {
        boolean exist = false;
        for (int i = 1; i < userList.size(); i++) {
            if (userList.get(i).getUsername() != null && userList.get(i).getUsername().equals(user.getUsername())) {
                exist = true;
            }
        }
        if (!exist) {
            userList.add(user);
            notifyItemInserted(getItemCount());
        }
        Application.addFriend(user);
    }

    public void addMember(ParseUser user) {
        userList.add(1, user);
        notifyItemInserted(1);
    }

    public class RecyclerHeadingViewHolder extends RecyclerView.ViewHolder {
        TextView heading;

        public RecyclerHeadingViewHolder(View itemView) {
            super(itemView);
            heading = (TextView) itemView.findViewById(R.id.heading);
        }
    }
}
