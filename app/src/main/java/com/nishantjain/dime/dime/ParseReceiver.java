package com.nishantjain.dime.dime;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.nishantjain.dime.dime.activities.SelectMeetingActivity;
import com.parse.GetCallback;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePushBroadcastReceiver;
import com.parse.ParseQuery;

import org.json.JSONException;
import org.json.JSONObject;

public class ParseReceiver extends ParsePushBroadcastReceiver {

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        super.onPushOpen(context, intent);

        ParseAnalytics.trackAppOpenedInBackground(intent);
        Intent launchActivity = new Intent(context, SelectMeetingActivity.class);
        launchActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(launchActivity);
        try {
            JSONObject data = new JSONObject(intent.getExtras().getString(
                    "com.parse.Data"));

            String meetingObjectId = data.getString("meeting");
            if (meetingObjectId != null && data.getString("action").equals("invite")) {
                ParseQuery<ParseObject> meetingQuery = ParseQuery.getQuery("Meeting");
                meetingQuery.include("meetingRole");
                meetingQuery.getInBackground(meetingObjectId, new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject meeting, ParseException e) {
                        Application.addToActiveMeetings(meeting);
                        if (SelectMeetingActivity.activeMeetingsAdapter != null) {
                            SelectMeetingActivity.activeMeetingsAdapter.notifyDataSetChanged();
                        }
                    }
                });
            } else if (data.getString("action").equals("finishMeeting")) {
                //End particular meeting or move it to archive
            } else {
                Toast.makeText(context, "Failed to receive meeting data :(", Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        super.onPushReceive(context, intent);
        try {
            JSONObject data = new JSONObject(intent.getExtras().getString(
                    "com.parse.Data"));

            if (data.getBoolean("change")) {
                Application.updateAvailable = true;
                PollFragment.setUpdateAvailable(true);
            } else {
                //new user joined meeting
                if (data.getString("action").equals("join")) {
                    FriendsFragment.userJoined(data.getString("creator"));
                }
            }
        } catch (JSONException e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

}