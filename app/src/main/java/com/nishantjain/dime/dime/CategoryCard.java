package com.nishantjain.dime.dime;

import android.content.Context;

import com.github.mikephil.charting.charts.PieChart;
import com.parse.ParseObject;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by Nishant on 6/18/2015.
 */
public class CategoryCard {
    private String category;
    private List<ParseObject> opinionList;
    private RecyclerAdapterForOpinion adapterForOpinionList;

    public CategoryCard(String category, ParseObject opinion, Context context){
        this.category = category;
        this.opinionList = new ArrayList<>();
        this.opinionList.add(opinion);
        this.adapterForOpinionList = new RecyclerAdapterForOpinion(context, this.opinionList);
    }

    public void addOpinion(ParseObject opinion) {
        this.opinionList.add(opinion);
    }

    public String getCategory() {
        return category;
    }

    public List<ParseObject> getOpinionList() {
        return opinionList;
    }

    public int getOpinionListSize(){
        return this.opinionList.size();
    }

    public RecyclerAdapterForOpinion getAdapterForOpinionList() {
        return adapterForOpinionList;
    }
}
