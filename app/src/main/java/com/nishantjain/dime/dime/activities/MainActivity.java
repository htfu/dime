package com.nishantjain.dime.dime.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Profile;
import com.nishantjain.dime.dime.Application;
import com.nishantjain.dime.dime.R;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends Activity implements View.OnClickListener {

    EditText Username, Password;
    String username;
    String password;
    Button Login;
    Button FacebookLogin;
    TextView CreateAccount;
    List<String> permissions;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_main);

        if (ParseUser.getCurrentUser() != null) {
            startSelectMeetingActivity();
        }

        Login = (Button) findViewById(R.id.loginButton);
        FacebookLogin = (Button) findViewById(R.id.facebookLoginButton);
        CreateAccount = (TextView) findViewById(R.id.CreateAccount);
        Username = (EditText) findViewById(R.id.username);
        Password = (EditText) findViewById(R.id.password);


        permissions = Arrays.asList("public_profile", "email", "user_friends");

//        //ANIMATIONS
//        YoYo.with(Techniques.RollIn)
//                .duration(1000)
//                .playOn(findViewById(R.id.appName));
//        YoYo.with(Techniques.FadeInUp)
//                .duration(4000)
//                .playOn(findViewById(R.id.loginButton));
//        YoYo.with(Techniques.FlipInX)
//                .duration(4000)
//                .playOn(findViewById(R.id.CreateAccount));

        FacebookLogin.setOnClickListener(this);
        CreateAccount.setOnClickListener(this);
        Login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.loginButton:
                login();
                break;
            case R.id.CreateAccount:
                Intent CreateAccount = new Intent(MainActivity.this,
                        com.nishantjain.dime.dime.CreateAccount.class);
                startActivity(CreateAccount);
                break;
            case R.id.facebookLoginButton:
                facebookLogin();
                break;
        }
    }

    private void facebookLogin() {
        ParseFacebookUtils.logInWithReadPermissionsInBackground(this, permissions, new LogInCallback() {
            @Override
            public void done(final ParseUser parseUser, ParseException e) {
                Application.downloadUserData(new Application.dataDownloadedCallback()
                {
                    @Override
                    public void dataAvailable()
                    {
                        ParseInstallation.getCurrentInstallation().put("user", parseUser.getUsername());
                        ParseUser.getCurrentUser().put("profilePicUrl", "https://graph.facebook.com/" + Profile.getCurrentProfile().getId() + "/picture?type=large");
                        ParseUser.getCurrentUser().put("Name", Profile.getCurrentProfile().getName());
                        ParseUser.getCurrentUser().saveInBackground();
                        ParseInstallation.getCurrentInstallation().saveInBackground();
                        startSelectMeetingActivity();
                    }

                    @Override
                    public void networkError()
                    {
                        Toast.makeText(getApplicationContext(), "Please check your internet connection!", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    private void startSelectMeetingActivity() {
        Intent SelectMeeting = new Intent(getApplicationContext(), SelectMeetingActivity.class);
        SelectMeeting.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(SelectMeeting);
    }

    private void login() {
        final ProgressDialog dlg = new ProgressDialog(this);
        dlg.setTitle("Please Wait: ");
        dlg.setMessage("Logging In, Please wait.");
        dlg.show();
        username = Username.getText().toString();
        password = Password.getText().toString();
        ParseUser.logInInBackground(username, password, new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if (e == null) {
                    //Adding username to device installation
                    ParseInstallation.getCurrentInstallation().put("user", username);
                    ParseUser.getCurrentUser().put("Name", username);
                    ParseUser.getCurrentUser().saveInBackground();
                    Application.currentUser = ParseUser.getCurrentUser();
                    ParseInstallation.getCurrentInstallation().saveInBackground();
                    Application.downloadUserData(new Application.dataDownloadedCallback() {
                        @Override
                        public void dataAvailable() {
                            startSelectMeetingActivity();
                        }

                        @Override
                        public void networkError() {
                            Toast.makeText(getApplicationContext(), "Please check your internet connection!", Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    Toast.makeText(getApplicationContext(), "Cannot login!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dlg.dismiss();
    }
}
