package com.nishantjain.dime.dime.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.nishantjain.dime.dime.Application;
import com.nishantjain.dime.dime.FriendsFragment;
import com.nishantjain.dime.dime.PollFragment;
import com.nishantjain.dime.dime.R;
import com.nishantjain.dime.dime.ViewPagerAdapter;
import com.ogaclejapan.smarttablayout.SmartTabLayout;


public class MeetingActivity extends ActionBarActivity {

    static ViewPager viewPager;
    private static SmartTabLayout tabStrip;
    private static Toolbar toolbar;
    private static View shadow;
    private ProgressDialog dialog;
    private Context activityContext;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {

            case android.R.id.home:
                Application.exitCurrentMeeting();
                NavUtils.navigateUpFromSameTask(this);
                break;
            case R.id.finish_meeting:
                meetingCleanUp();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_meeting, menu);
        if (!Application.currentMeeting.get("createdBy").equals(Application.currentUser.getString("Name"))) {
            menu.removeItem(R.id.finish_meeting);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.meeting_activity_main);

        activityContext = this;
        toolbar = (Toolbar) findViewById(R.id.appBar);
        shadow = findViewById(R.id.shadow);
        toolbar.setTitle(Application.currentMeeting.getString("meetingName"));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        FragmentManager fragmentManager = getSupportFragmentManager();
        String[] TITLES = {"Poll", "People"};
        viewPager.setAdapter(new ViewPagerAdapter(fragmentManager, TITLES, new PollFragment(), new FriendsFragment()));
        // Bind the tabs to the ViewPager
        tabStrip = (SmartTabLayout) findViewById(R.id.viewPagerTab);
        tabStrip.setViewPager(viewPager);

        tabStrip.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                PollFragment.fabCreateTopic.setTranslationX(-positionOffset);
                PollFragment.fabCreateTopic.animate();
            }

            @Override
            public void onPageSelected(int position) {
                MeetingActivity.showToolbar();
                if (position == 0) {
                    PollFragment.fabCreateTopic.setVisibility(View.VISIBLE);
                } else {
                    FriendsFragment.inviteFriends.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    protected void meetingCleanUp() {
        dialog = new ProgressDialog(activityContext);
        dialog.setMessage("Processing decision..");
        dialog.show();
        if (PollFragment.categoryCards != null) {
            Application.processOpinionsForDecision(PollFragment.categoryCards, dialog, new Application.meetingSyncedCallback() {
                @Override
                public void syncDone() {
                    Application.endMeeting(dialog, new Application.meetingSyncedCallback() {
                        @Override
                        public void syncDone() {
                            Intent DecisionViewActivity = new Intent(getApplicationContext(), DecisionViewActivity.class);
                            startActivity(DecisionViewActivity);
                            Application.notifyUsers(Application.meetingUsers, "finishMeeting");
                        }
                    });
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        Application.exitCurrentMeeting();
        super.onBackPressed();
    }

    public static void hideToolbar() {
        tabStrip.animate().translationY(-toolbar.getHeight()).setInterpolator(new AccelerateInterpolator(2));
        shadow.animate().translationY(-toolbar.getHeight()).setInterpolator(new AccelerateInterpolator(2));
        toolbar.animate().translationY(-toolbar.getHeight()).setInterpolator(new AccelerateInterpolator(3));
    }

    public static void showToolbar() {
        tabStrip.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
        shadow.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
        toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(3));
    }

    @Override
    protected void onPause() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
        super.onPause();
    }
}
