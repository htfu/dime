package com.nishantjain.dime.dime.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nishantjain.dime.dime.Application;
import com.nishantjain.dime.dime.R;
import com.parse.ParseUser;

import org.solovyev.android.views.llm.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

public class SearchUsersActivity extends AppCompatActivity implements RecyclerAdapterForUser.onSelectListener
{

    static int SEARCH_FAILED = 1;
    static int SEARCH_SUCCESSFUL = 2;
    static ParseUser selectedUser;
    private List<ParseUser> userList;
    private RecyclerAdapterForUser recyclerAdapterForUser;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search_users);
        Toolbar searchBar = (Toolbar) findViewById(R.id.search_bar);
        final EditText search = (EditText) searchBar.findViewById(R.id.searchEditText);
        // Request focus and show soft keyboard automatically
        search.requestFocus();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);

        userList = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        setSupportActionBar(searchBar);

        search.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch(search.getText().toString());
                    return true;
                }
                return false;
            }
        });
    }

    private void performSearch(String text) {
        if (text == null) {
            Toast.makeText(this, "Enter a search query", Toast.LENGTH_SHORT).show();
        } else {
            ProgressDialog dialog = new ProgressDialog(this);
            dialog.setMessage("Searching..");
            dialog.show();
            Application.findUser(text, dialog, new Application.searchCallback()
            {
                @Override
                public void dataAvailable(List<ParseUser> list)
                {
                    userList = list;
                    if (userList == null || userList.isEmpty())
                    {
                        Toast.makeText(getApplicationContext(), "Search failed. Try different search term", Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        recyclerAdapterForUser = new RecyclerAdapterForUser(userList, SearchUsersActivity.this);
                        recyclerAdapterForUser.setSelectionListener(SearchUsersActivity.this);
                        recyclerView.setAdapter(recyclerAdapterForUser);
                    }
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(SEARCH_FAILED);
        finish();
    }

    @Override
    public void onSelect(ParseUser user) {
        selectedUser = user;
        setResult(SEARCH_SUCCESSFUL);
        finish();
    }
}
