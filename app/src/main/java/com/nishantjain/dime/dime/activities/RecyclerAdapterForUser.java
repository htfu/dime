package com.nishantjain.dime.dime.activities;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nishantjain.dime.dime.R;
import com.parse.ParseUser;
import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Nishant on 7/24/2015.
 */
class RecyclerAdapterForUser extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ParseUser> userList;
    private Context context;

    private onSelectListener listener;

    public RecyclerAdapterForUser(List<ParseUser> userList, Context context) {
        this.userList = userList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_item_layout, parent, false);
        return new UserViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        final UserViewHolder userViewHolder = (UserViewHolder) viewHolder;
        final ParseUser current = userList.get(position);
        userViewHolder.Name.setText(current.getString("Name"));
        userViewHolder.username.setText(current.getString("username"));
        if (current.getString("profilePicUrl") != null) {
            Picasso.with(context).load(current.getString("profilePicUrl")).into(userViewHolder.userProfile);
        } else {
            userViewHolder.userProfile.setImageResource(R.drawable.com_facebook_profile_picture_blank_portrait);
        }
        userViewHolder.check.setVisibility(View.GONE);
        userViewHolder.primaryFunction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onSelect(userList.get(position));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (userList != null) {
            return userList.size();
        } else {
            return 0;
        }
    }

    public void setSelectionListener(onSelectListener listener) {
        this.listener = listener;
    }

    public interface onSelectListener {
        void onSelect(ParseUser user);
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {

        TextView Name, username;
        CircularImageView userProfile;
        CheckBox check;
        RelativeLayout primaryFunction;

        public UserViewHolder(View itemView) {
            super(itemView);
            Name = (TextView) itemView.findViewById(R.id.Name);
            username = (TextView) itemView.findViewById(R.id.username);
            userProfile = (CircularImageView) itemView.findViewById(R.id.userProfile);
            check = (CheckBox) itemView.findViewById(R.id.check);
            primaryFunction = (RelativeLayout) itemView.findViewById(R.id.primaryFunction);
        }
    }
}
