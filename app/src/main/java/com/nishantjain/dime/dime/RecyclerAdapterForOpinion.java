package com.nishantjain.dime.dime;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nishant on 6/2/2015.
 */
public class RecyclerAdapterForOpinion extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ParseObject> opinionList;
    private Context context;
    private int expandedPosition = -1;
    private String currentUsername = Application.currentUser.getString("Name");
    private PieChart pieChart;

    public RecyclerAdapterForOpinion(Context context, List<ParseObject> opinionList) {
        this.opinionList = opinionList;
        this.context = context;
    }

    public void setPieChart(PieChart pieChart) {
        this.pieChart = pieChart;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_view_poll, viewGroup, false);
        return new OpinionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        ParseObject current = opinionList.get(position);
        List<String> userLikes = (List<String>) current.get("userLike");
        List<String> userUnlikes = (List<String>) current.get("userUnlike");
        List<ParseUser> users = Application.meetingUsers;
        OpinionViewHolder opinionViewHolder = (OpinionViewHolder) viewHolder;
        opinionViewHolder.entry.setText(current.getString("entry"));
        opinionViewHolder.rationale.setText(current.getString("rationale"));
        if (position < 5) {
            opinionViewHolder.indicator.setBackgroundColor(ColorTemplate.VORDIPLOM_COLORS[position]);
        }
        opinionViewHolder.commentRecyclerView.setLayoutManager(new org.solovyev.android.views.llm.LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));

//        if (userLikes != null) {
//
//            List<ParseUser> userLike = new ArrayList<>();
//            getUserProfiles(userLikes, users, userLike);
//
//            opinionViewHolder.userLikes.setText("" + userLikes.size());
//            if (userLikes.size() != 0 && userLikes.contains(currentUsername)) {
//                opinionViewHolder.like.setChecked(true);
//            } else {
//                opinionViewHolder.like.setChecked(false);
//            }
//            RecyclerAdapterForUser likeAdapter = new RecyclerAdapterForUser(userLike, context);
//            opinionViewHolder.likeRecyclerView.setAdapter(likeAdapter);
//        }
//
//        if (userUnlikes != null) {
//
//            List<ParseUser> userUnlike = new ArrayList<>();
//            getUserProfiles(userUnlikes, users, userUnlike);
//
//            opinionViewHolder.userUnlikes.setText("" + userUnlikes.size());
//            if (userUnlikes.size() != 0 && userUnlikes.contains(currentUsername)) {
//                opinionViewHolder.unlike.setChecked(true);
//            } else {
//                opinionViewHolder.unlike.setChecked(false);
//            }
//            RecyclerAdapterForUser unlikeAdapter = new RecyclerAdapterForUser(userUnlike, context);
//            opinionViewHolder.unlikeRecyclerView.setAdapter(unlikeAdapter);
//        }

        if (Application.meetingUsers != null && opinionList.size() > 2 && current.getInt("priority") < -1 * (Application.meetingUsers.size() * 0.6)) {
            opinionViewHolder.layoutView.setAlpha((float) 0.30);
        } else {
            opinionViewHolder.layoutView.setAlpha((float) 1);
        }

        if (position == expandedPosition) {
            opinionViewHolder.expandedLayout.setVisibility(View.VISIBLE);
            opinionViewHolder.likeUnlikeView.setImageResource(R.drawable.ic_collapse);
        } else {
            opinionViewHolder.expandedLayout.setVisibility(View.GONE);
            opinionViewHolder.likeUnlikeView.setImageResource(R.drawable.ic_action);
        }
    }

    private void getUserProfiles(List<String> userLikes, List<ParseUser> users, List<ParseUser> userLike) {
        for (int i = 0; i < userLikes.size(); i++) {
            for (int j = 0; j < users.size(); j++) {
                String userName = userLikes.get(i);
                ParseUser user = users.get(j);
                if (user.getString("Name").equals(userName)) {
                    userLike.add(user);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return this.opinionList.size();
    }

    public void addItem(ParseObject opinion) {
        this.opinionList.add(opinion);
        notifyItemInserted(getItemCount() - 1);
        pieChart.notifyDataSetChanged();
        pieChart.invalidate();
    }

    public class OpinionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView entry;
        TextView rationale;
        TextView userLikes;
        TextView userUnlikes;
        CheckBox like, unlike;
        ImageView likeUnlikeView;
        View indicator;
        RelativeLayout expandedLayout;
        RecyclerView commentRecyclerView;
        RelativeLayout layoutView;

        public OpinionViewHolder(View itemView) {
            super(itemView);
            entry = (TextView) itemView.findViewById(R.id.opinionEntry);
            like = (CheckBox) itemView.findViewById(R.id.opinionLike);
            unlike = (CheckBox) itemView.findViewById(R.id.opinionUnlike);
            rationale = (TextView) itemView.findViewById(R.id.opinionRationale);
            userLikes = (TextView) itemView.findViewById(R.id.userLikes);
            userUnlikes = (TextView) itemView.findViewById(R.id.userUnlikes);
            likeUnlikeView = (ImageView) itemView.findViewById(R.id.likeUnlikeArrow);
            expandedLayout = (RelativeLayout) itemView.findViewById(R.id.expandedItemLayout);
            commentRecyclerView = (RecyclerView) itemView.findViewById(R.id.commentRecyclerView);
            layoutView = (RelativeLayout) itemView.findViewById(R.id.layoutView);
            indicator = itemView.findViewById(R.id.indicator);

            like.setOnClickListener(this);
            unlike.setOnClickListener(this);
            likeUnlikeView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int id = v.getId();
            switch (id) {
                case R.id.opinionLike:
                    ParseObject current = opinionList.get(getAdapterPosition());
                    CheckBox check = (CheckBox) v;
                    if (check.isChecked()) {
                        Application.likeOpinion(current);
                    } else {
                        Application.removeLike(current);
                    }
                    Application.removeUnlike(current);
                    notifyItemChanged(getAdapterPosition());
                    Application.silentNotifyChange();
                    pieChart.getData().getDataSet().getEntryForXIndex(getAdapterPosition()).setVal(current.getInt("priority"));
                    pieChart.invalidate();
                    break;
                case R.id.opinionUnlike:
                    ParseObject mCurrent = opinionList.get(getAdapterPosition());
                    CheckBox mCheck = (CheckBox) v;
                    if (mCheck.isChecked()) {
                        Application.unlikeOpinion(mCurrent);
                    } else {
                        Application.removeUnlike(mCurrent);
                    }
                    Application.removeLike(mCurrent);
                    notifyItemChanged(getAdapterPosition());
                    Application.silentNotifyChange();
                    pieChart.getData().getDataSet().getEntryForXIndex(getAdapterPosition()).setVal(mCurrent.getInt("priority"));
                    pieChart.invalidate();
                    break;
                case R.id.likeUnlikeArrow:
                    // Check for an expanded view, collapse if you find one
                    if (expandedPosition >= 0) {
                        int prev = expandedPosition;
                        notifyItemChanged(prev);
                    }
                    // Set the current position to "expanded"
                    if (expandedPosition == getAdapterPosition()) {
                        expandedPosition = -1;
                    } else {
                        expandedPosition = getAdapterPosition();
                        notifyItemChanged(expandedPosition);
                    }
                    break;
            }
        }
    }
}



