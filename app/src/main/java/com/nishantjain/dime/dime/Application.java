package com.nishantjain.dime.dime;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;
import com.nishantjain.dime.dime.activities.SelectMeetingActivity;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseRole;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Application extends android.app.Application {

    public static ParseUser currentUser;
    public static ParseRole meetingRole;
    public static List<ParseObject> activeMeetings = new ArrayList<>();
    public static List<ParseObject> archivedMeetings = new ArrayList<>();
    public static List<ParseObject> opinionList;
    public static ParseObject currentMeeting;
    public static dataDownloadedCallback callback;
    public static List<ParseObject> decisionOpinionList;
    public static List<ParseUser> meetingUsers;
    public static boolean updateAvailable = false;
    public static List<ParseUser> friends;

    public Application() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Initialize the Parse SDK.

        Parse.enableLocalDatastore(this);
        Parse.initialize(this.getApplicationContext(), getString(R.string.app_id),
                getString(R.string.client_id));
        ParseFacebookUtils.initialize(getApplicationContext());
        ParseInstallation.getCurrentInstallation().saveInBackground();
    }

    public static void joinMeeting(final ParseObject meeting, final MaterialDialog dialog, final meetingSyncedCallback callback) {
        currentMeeting = meeting;
        currentUser.put("inMeeting", meeting);
        currentUser.addUnique("activeMeetings", meeting);
        meetingRole = (ParseRole) meeting.get("meetingRole");
        if (meetingRole != null) {
            meetingRole.getUsers().add(currentUser);
            meetingRole.saveInBackground();
        }
        if (!ParseInstallation.getCurrentInstallation().getList("channels").contains(meeting.getObjectId())) {
            ParsePush.subscribeInBackground(meeting.getObjectId());
            ParseInstallation.getCurrentInstallation().saveInBackground();
        }

        ParsePush push = new ParsePush();
        JSONObject data = new JSONObject();
        try {
            data.put("change", false);
            data.put("action", "join");
            data.put("creator", currentUser.getUsername());
            data.put("meeting", Application.currentMeeting.getObjectId());
        } catch (JSONException e) {
        }

        // Setting expiration of push to 1 hour
        long timeInterval = 60 * 60;
        push.setChannel(meeting.getObjectId());
        push.setExpirationTimeInterval(timeInterval);
        push.setData(data);
        ParseQuery<ParseInstallation> userParseQuery = ParseInstallation.getQuery();
        userParseQuery.whereNotEqualTo("user", currentUser.getUsername());
        push.setQuery(userParseQuery);
        push.sendInBackground();

        currentUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                downloadMeetingOpinionList(dialog, new dataDownloadedCallback() {
                    @Override
                    public void dataAvailable() {
                        dialog.dismiss();
                        callback.syncDone();
                    }

                    @Override
                    public void networkError() {
                        Log.d("ERROR:", " Network Connection Lost!!");
                    }
                });
            }
        });
    }

    public static void deleteMeeting(ParseObject meeting) {
        activeMeetings.remove(meeting);
        ParseRole role = (ParseRole) meeting.get("meetingRole");
        if (role != null) {
            role.getUsers().remove(currentUser);
            role.saveInBackground();
        }
        List<ParseObject> temp = new ArrayList<>();
        temp.add(meeting);
        ParsePush.unsubscribeInBackground(meeting.getObjectId());
        ParseInstallation.getCurrentInstallation().saveInBackground();
        currentUser.removeAll("activeMeetings", temp);
        currentUser.saveInBackground();
        if (meeting.getString("createdBy").equals(currentUser.getString("Name"))) {
            meeting.deleteInBackground();
            if (role != null) {
                role.deleteInBackground();
            }
        }
    }

    public static void silentNotifyChange() {
        if (!updateAvailable) {
            ParsePush push = new ParsePush();
            ParseQuery<ParseInstallation> userParseQuery = ParseInstallation.getQuery();
            userParseQuery.whereNotEqualTo("user", currentUser.getUsername());

            push.setChannel(Application.currentMeeting.getObjectId());
            push.setQuery(userParseQuery);
            JSONObject data = new JSONObject();

            try {
                data.put("change", true);
                data.put("creator", currentUser.getUsername());
                data.put("meeting", Application.currentMeeting.getObjectId());
            } catch (JSONException e) {
            }

            // Setting expiration of push to 1 hour
            long timeInterval = 60 * 60;
            push.setExpirationTimeInterval(timeInterval);
            push.setData(data);
            push.sendInBackground();
        }
    }

    public static void downloadMeetingOpinionList(final MaterialDialog dialog,
                                                  final dataDownloadedCallback callback) {
        if (currentMeeting != null) {
            final ParseQuery<ParseObject> meetingQuery = ParseQuery.getQuery("Meeting");
            meetingQuery.include("meetingRole");
            meetingQuery.include("opinionList");
            meetingQuery.include("opinionList.userLike");
            meetingQuery.include("opinionList.userUnlike");
            meetingQuery.getInBackground(currentMeeting.getObjectId(), new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    currentMeeting = parseObject;
                    meetingRole = (ParseRole) currentMeeting.get("meetingRole");
                    meetingRole.getUsers().getQuery().findInBackground(new FindCallback<ParseUser>() {
                        @Override
                        public void done(List<ParseUser> list, ParseException e) {
                            if (list != null) {
                                meetingUsers = list;
                            }
                            callback.dataAvailable();
                            dialog.dismiss();
                        }
                    });
                }
            });
        }
    }

    public static ParseObject createParseObject(String category, String entry, String
            rationale) {
        ParseObject opinion = new ParseObject("opinion");
        opinion.put("category", category);
        opinion.put("entry", entry);
        opinion.put("rationale", rationale);
        opinion.put("priority", 0);
        opinion.put("user", Application.currentUser);
        opinion.put("userLike", new ArrayList<>());
        opinion.put("userUnlike", new ArrayList<>());

        ParseACL groupACL = new ParseACL();
        groupACL.setRoleWriteAccess(meetingRole, true);
        groupACL.setRoleReadAccess(meetingRole, true);
        opinion.setACL(groupACL);
        opinion.saveInBackground();

        currentMeeting.addUnique("opinionList", opinion);
        currentMeeting.saveInBackground();

        return opinion;
    }

    public static void likeOpinion(ParseObject opinion) {
        opinion.put("priority", opinion.getInt("priority") + 1);
        opinion.addUnique("userLike", currentUser.getString("Name"));
        opinion.saveInBackground();
    }

    public static void unlikeOpinion(ParseObject opinion) {
        opinion.put("priority", opinion.getInt("priority") - 1);
        opinion.addUnique("userUnlike", currentUser.getString("Name"));
        opinion.saveInBackground();
    }

    public static void removeLike(ParseObject opinion) {
        if (opinion.getList("userLike").contains(currentUser.getString("Name"))) {
            List<String> temp = new ArrayList<>();
            temp.add(currentUser.getString("Name"));
            opinion.removeAll("userLike", temp);
            opinion.put("priority", opinion.getInt("priority") - 1);
            opinion.saveInBackground();
        }
    }

    public static void removeUnlike(ParseObject opinion) {
        if (opinion.getList("userUnlike").contains(currentUser.getString("Name"))) {
            List<String> temp = new ArrayList<>();
            temp.add(currentUser.getString("Name"));
            opinion.put("priority", opinion.getInt("priority") + 1);
            opinion.removeAll("userUnlike", temp);
            opinion.saveInBackground();
        }
    }

    public static void processOpinionsForDecision(ArrayList<CategoryCard> categoryCards, final ProgressDialog dialog, final meetingSyncedCallback callback) {
        decisionOpinionList = new ArrayList<>();
        for (int i = 0; i < categoryCards.size(); i++) {
            decisionOpinionList.add(categoryCards.get(i).getOpinionList().get(0));
        }
        currentMeeting.put("decisionOpinionList", decisionOpinionList);
        currentMeeting.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                callback.syncDone();
                dialog.dismiss();
            }
        });
    }

    public static void addToActiveMeetings(ParseObject meeting) {
        activeMeetings.add(meeting);
        currentUser.addUnique("activeMeetings", meeting);
        currentUser.saveInBackground();
    }

    public static void findDecision(ParseObject meetingToDisplay,
                                    final dataDownloadedCallback callback) {
        ParseQuery<ParseObject> meetingQuery = ParseQuery.getQuery("Meeting");
        meetingQuery.include("decisionOpinionList");
        meetingQuery.getInBackground(meetingToDisplay.getObjectId(), new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject meeting, ParseException e) {
                decisionOpinionList = meeting.getList("decisionOpinionList");
                callback.dataAvailable();
            }
        });
    }

    public static void notifyUsers(List<ParseUser> userList, @NonNull String alert) {
        // Associate a query with the installation
        ParseQuery<ParseInstallation> pushQuery = ParseInstallation.getQuery();
        ParsePush push = new ParsePush();

        for (int i = 0; i < userList.size(); i++) {
            pushQuery.whereEqualTo("user", userList.get(i).getUsername());
        }

        push.setQuery(pushQuery);
        JSONObject data = new JSONObject();

        try {
            data.put("action", alert);

            if (alert.equals("invite")) {
                data.put("alert", currentUser.getString("Name")
                        + " invited you to join a group meeting.");
            } else if (alert.equals("finishMeeting")) {
                data.put("alert", currentUser.getString("Name") + " has ended the meeting. Decision of the meeting will be available soon.");
            }

            data.put("title", "Dime");
            data.put("change", false);
            data.put("creator", currentUser.getUsername());
            data.put("meeting", currentMeeting.getObjectId());
        } catch (JSONException e) {
            Log.d("ParseException", e.getMessage());
        }

        // Setting expiration of push to 2 hour
        long timeInterval = 60 * 120;
        push.setExpirationTimeInterval(timeInterval);
        push.setData(data);
        push.sendInBackground();
    }

    public static void findUser(String text, final ProgressDialog dialog, final searchCallback callback) {
        ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
        userParseQuery.whereContains("Name", text);
        userParseQuery.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> userList, ParseException e) {
                callback.dataAvailable(userList);
                dialog.dismiss();
            }
        });
    }

    public static void findUser(String text, final searchSingleCallback callback) {
        ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
        userParseQuery.whereEqualTo("username", text);
        userParseQuery.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                callback.dataAvailable(parseUser);
            }
        });
    }

    public static void addFriend(ParseUser user) {
        currentUser.addUnique("friends", user);
        if (friends != null && !friends.contains(user))
            friends.add(user);
        currentUser.saveInBackground();
    }

    public interface dataDownloadedCallback {
        void dataAvailable();

        void networkError();
    }

    public interface searchCallback {
        void dataAvailable(List<ParseUser> userList);
    }

    public interface searchSingleCallback {
        void dataAvailable(ParseUser user);
    }

    public interface meetingSyncedCallback {
        void syncDone();
    }

    public static void downloadUserData(final dataDownloadedCallback downloadCallback) {

        callback = downloadCallback;
        currentUser = ParseUser.getCurrentUser();
        ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
        userQuery.whereEqualTo("username", currentUser.getUsername());
        userQuery.include("activeMeetings");
        userQuery.include("archivedMeetings");
        userQuery.include("inMeeting");
        userQuery.include("friends");
        userQuery.include("inMeeting.meetingRole");
        userQuery.include("activeMeetings.meetingRole");
        userQuery.getFirstInBackground(new GetCallback<ParseUser>() {

            @Override
            public void done(ParseUser object, ParseException e) {
                if (e == null) {
                    currentUser = object;
                    activeMeetings = currentUser.getList("activeMeetings");
                    archivedMeetings = currentUser.getList("archivedMeetings");
                    if (activeMeetings == null) {
                        activeMeetings = new ArrayList<>();
                    }
                    if (archivedMeetings == null) {
                        archivedMeetings = new ArrayList<>();
                    }
                    int size = archivedMeetings.size();
                    for (int j = 0; j < size; j++) {
                        archivedMeetings.set(j, archivedMeetings.get(size - (j + 1)));
                    }
                    friends = currentUser.getList("friends");
                    if (currentUser.get("inMeeting") != null) {
                        currentMeeting = (ParseObject) currentUser.get("inMeeting");
                        opinionList = currentMeeting.getList("opinionList");
                        meetingRole = (ParseRole) currentMeeting.get("meetingRole");
                        callback.dataAvailable();
                    } else {
                        callback.dataAvailable();
                    }
                } else {
                    Log.d("Parse Error:", e.getMessage());
                    downloadCallback.networkError();
                }

            }
        });
    }

    public static void startMeeting(String type, String meetingName, String meetingObjective,
                                    final MaterialDialog dialog, final meetingSyncedCallback callback) {

        currentMeeting = new ParseObject("Meeting");
        currentMeeting.put("createdBy", currentUser.getString("Name"));
        currentMeeting.put("type", type);
        currentMeeting.put("meetingName", meetingName);
        currentMeeting.put("meetingObjective", meetingObjective);
        currentMeeting.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                ParseACL roleACL = new ParseACL();
                roleACL.setPublicReadAccess(true);
                roleACL.setPublicWriteAccess(true);
                meetingRole = new ParseRole(currentMeeting.getObjectId(), roleACL);
                meetingRole.getUsers().add(currentUser);
                meetingRole.saveInBackground();
                currentMeeting.put("meetingRole", meetingRole);
                callback.syncDone();
                dialog.dismiss();
            }
        });
        currentUser.put("inMeeting", currentMeeting);
        currentUser.addUnique("activeMeetings", currentMeeting);
        currentUser.saveInBackground();
        if (activeMeetings == null) {
            activeMeetings = new ArrayList<>();
            activeMeetings.add(currentMeeting);
        } else {
            activeMeetings.add(currentMeeting);
        }
    }

    public static void exitCurrentMeeting() {
        currentUser.remove("inMeeting");
        currentUser.saveInBackground();
        currentMeeting = null;
        meetingRole = null;
    }

    public static void endMeeting(final ProgressDialog dialog,
                                  final meetingSyncedCallback callback) {
        currentUser.addUnique("archivedMeetings", currentMeeting);
        currentUser.remove("inMeeting");
        if (archivedMeetings == null) {
            archivedMeetings = new ArrayList<>();
        }
        archivedMeetings.add(archivedMeetings.get(archivedMeetings.size() - 1));
        for (int j = (archivedMeetings.size() - 2); j >= 0; j--) {
            archivedMeetings.set(j + 1, archivedMeetings.get(j));
        }
        archivedMeetings.set(0, currentMeeting);
        currentUser.addUnique("archivedMeetings", currentMeeting);
        List<ParseObject> temp = new ArrayList<>();
        temp.add(currentMeeting);
        for (int i = 0; i < activeMeetings.size(); i++) {
            if (activeMeetings.get(i).getObjectId().equals(currentMeeting.getObjectId())) {
                activeMeetings.remove(i);
            }
        }
        currentUser.removeAll("activeMeetings", temp);
        currentUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                callback.syncDone();
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });
        currentMeeting = null;
        meetingRole = null;
        if (SelectMeetingActivity.activeMeetingsAdapter != null) {
            SelectMeetingActivity.activeMeetingsAdapter.notifyDataSetChanged();
        }
        if (SelectMeetingActivity.archivedMeetingsAdapter != null) {
            SelectMeetingActivity.archivedMeetingsAdapter.notifyDataSetChanged();
        }
    }

}