package com.nishantjain.dime.dime;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.datetimepicker.time.RadialPickerLayout;
import com.android.datetimepicker.time.TimePickerDialog;
import com.parse.ParseObject;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;

/**
 * Created by Nishant on 5/27/2015.
 */
public class TopicDialog extends DialogFragment implements View.OnClickListener, Spinner.OnItemSelectedListener, TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {
    ParseObject opinion;
    OnTopicCreatedListener listener;
    private EditText topicEntry;
    private EditText topicRationale;
    private String selectedCategory;
    private LinearLayout layoutForOptions;
    private EditText customCategory;
    private Button addTime;
    private Button addDate;
    private Calendar now;
    private View.OnClickListener extraOptionsClickListener;
    private int initCategoryPosition = 0;
    private Button addRestaurant;
    private String topicCategory;

    static TopicDialog newInstance() {
        return new TopicDialog();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        int style = DialogFragment.STYLE_NO_TITLE, theme = 0;
        setStyle(style, theme);

        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.topic_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.createNewTopic) {
            createOpinionObject();
        }
        return super.onOptionsItemSelected(item);
    }

    void setCallbackListener(OnTopicCreatedListener listener) {
        this.listener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.topic_dialog, container, false);

        Spinner category = (Spinner) v.findViewById(R.id.category);
        topicEntry = (EditText) v.findViewById(R.id.entry);
        topicRationale = (EditText) v.findViewById(R.id.rationale);
        //Button createTopic = (Button) v.findViewById(R.id.createTopic);
        layoutForOptions = (LinearLayout) v.findViewById(R.id.layoutForOptions);
        Button createTopic = (Button) v.findViewById(R.id.createTopic);
        Button cancelTopic = (Button) v.findViewById(R.id.cancelTopic);

        createTopic.setOnClickListener(this);
        cancelTopic.setOnClickListener(this);

        //createTopic.setOnClickListener(this);
        category.setOnItemSelectedListener(this);

        if (initCategoryPosition > 0) {
            category.setSelection(initCategoryPosition, true);
        }
        setExtraOptionsOnClickListener();

        return v;
    }

    private void setExtraOptionsOnClickListener() {
        extraOptionsClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tag = (String) v.getTag();
                switch (tag) {
                    case "date":
                        now = Calendar.getInstance();
                        DatePickerDialog dpd = DatePickerDialog.newInstance(
                                TopicDialog.this,
                                now.get(Calendar.YEAR),
                                now.get(Calendar.MONTH),
                                now.get(Calendar.DAY_OF_MONTH)
                        );
                        dpd.show(getActivity().getFragmentManager(), "DatePickerDialog");
                        break;

                    case "time":
                        now = Calendar.getInstance();
                        TimePickerDialog tpd = TimePickerDialog.newInstance(
                                TopicDialog.this,
                                now.get(Calendar.HOUR_OF_DAY),
                                now.get(Calendar.MINUTE),
                                false
                        );
                        tpd.show(getActivity().getFragmentManager(), "TimePickerDialog");
                        break;

                    case "restaurant":
                        pickRestaurant();
                }
            }
        };
    }

    private void pickRestaurant() {
        Intent mapIntent = new Intent(Intent.ACTION_VIEW);
        mapIntent.setData(Uri.parse("geo:0,0?q=restaurants"));
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.createTopic:
                createOpinionObject();
                break;
            case R.id.cancelTopic:
                TopicDialog.this.dismiss();
                break;
        }
    }

    private void createOpinionObject() {
        String entry = topicEntry.getText().toString();
        String rationale = topicRationale.getText().toString();
        if (customCategory != null) {
            selectedCategory = customCategory.getText().toString();
        }
        if (entry.isEmpty() || selectedCategory == null || selectedCategory.equals("Select Category") || selectedCategory.isEmpty()) {
            Toast.makeText(getActivity(), "Please fill the data.", Toast.LENGTH_SHORT).show();
        } else {
            opinion = Application.createParseObject(selectedCategory, entry, rationale);
            listener.onTopicDialogSubmitted(opinion);
            TopicDialog.this.dismiss();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        selectedCategory = parent.getItemAtPosition(position).toString();

        if (selectedCategory.equals("Custom") && customCategory == null) {
            customCategory = new EditText(getActivity());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(0, 20, 0, 0);
            customCategory.setLayoutParams(params);
            customCategory.setHint("Custom Category");
            customCategory.setTextSize(14);
            if (topicCategory != null) {
                customCategory.setText(topicCategory);
            }
            layoutForOptions.addView(customCategory);
        } else if (!selectedCategory.equals("Custom")) {
            layoutForOptions.removeView(customCategory);
            customCategory = null;
        }

        if (selectedCategory.equals("Date and Time") && addDate == null) {
            addTime = new Button(getActivity());
            addDate = new Button(getActivity());
            addDate.setTag("date");
            addTime.setTag("time");
            addTime.setText("Pick Time");
            addDate.setText("Pick Date");
            layoutForOptions.addView(addDate);
            layoutForOptions.addView(addTime);
            addDate.setOnClickListener(extraOptionsClickListener);
            addTime.setOnClickListener(extraOptionsClickListener);
        } else if (!selectedCategory.equals("Date and Time")) {
            layoutForOptions.removeView(addDate);
            layoutForOptions.removeView(addTime);
            addTime = null;
            addDate = null;
        }

        if (selectedCategory.equals("Restaurant") && addRestaurant == null) {
            addRestaurant = new Button(getActivity());
            addRestaurant.setText("Find Nearby Restaurants");
            layoutForOptions.addView(addRestaurant);
            addRestaurant.setTag("restaurant");
            addRestaurant.setOnClickListener(extraOptionsClickListener);
        } else if (!selectedCategory.equals("Restaurant")) {
            layoutForOptions.removeView(addRestaurant);
            addRestaurant = null;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar now = Calendar.getInstance();
        int curDay = now.get(Calendar.DAY_OF_MONTH);
        int curMonth = now.get(Calendar.MONTH);
        int curYear = now.get(Calendar.YEAR);
        String date;
        String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        if (curMonth == monthOfYear && curYear == year) {
            if (dayOfMonth == curDay)
                date = "Today";
            else if (dayOfMonth == curDay + 1) {
                date = "Tomorrow";
            } else if (dayOfMonth == curDay - 1) {
                date = "Yesterday";
            } else {
                date = dayOfMonth + "-" + months[monthOfYear];
            }
        } else {
            date = dayOfMonth + "-" + months[monthOfYear];
        }
        if (topicEntry.getText().toString().isEmpty()) {
            topicEntry.append(date);
        } else {
            topicEntry.append("\n" + date);
        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String timeOfDay;
        if (hourOfDay < 12) {
            timeOfDay = "AM";
        } else {
            hourOfDay = hourOfDay - 12;
            timeOfDay = "PM";
        }
        String time = hourOfDay + ":" + minuteString + " " + timeOfDay;
        if (topicEntry.getText().toString().isEmpty()) {
            topicEntry.append(time);
        } else {
            topicEntry.append("\n" + time);
        }
    }

    public void initializeCategorySpinner(int position) {
        this.initCategoryPosition = position;
    }

    public void initializeCategorySpinner(String customCategory) {
        this.initCategoryPosition = 4;
        this.topicCategory = customCategory;
    }

    public interface OnTopicCreatedListener {
        void onTopicDialogSubmitted(ParseObject opinion);
    }
}